use DB_asterix;

-- 1 Liste des potions : Numéro, libellé, formule et constituant principal. (5 lignes)
SELECT * FROM potion;

-- 2 Liste des noms des trophées rapportant 3 points. (2 lignes)
-- Use table CATEGORIE (CodeCat, NomTrophee, NbPoints)
SELECT * FROM categorie;
SELECT NomCateg, NbPoints FROM categorie WHERE NbPoints=3;


SELECT "Liste des villages (noms) contenant plus de 35 huttes" AS "Question 3";

SELECT * FROM village;
SELECT NomVillage, NbHuttes FROM village where NbHuttes>35;


SELECT "Liste des trophées (numéros) pris en mai / juin 52. (4 lignes)" AS "Qestion 4";

SELECT NumTrophee, DatePrise FROM trophee WHERE DatePrise BETWEEN "2052-05-01" AND "2052-06-30";
DESCRIBE trophee;
SELECT MONTH("2052-06-24");

SELECT "Noms des habitants commençant par 'a' et contenant la lettre 'r'. (3 lignes)" AS "Question 5";
SELECT Nom FROM habitant 
WHERE Nom LIKE "a%r%";


SELECT "Numéros des habitants ayant bu les potions numéros 1, 3 ou 4. (8 lignes)" as "Question 6";

SELECT DISTINCT (NumHab) from absorber where NumPotion IN (1, 3, 4);

SELECT "Liste des trophées : numéro, date de prise, nom de la catégorie et nom du preneur. (10
lignes)" AS "Question 7";

SELECT trophee.NumTrophee, trophee.DatePrise, categorie.NomCateg, habitant.nom
FROM trophee
JOIN categorie ON trophee.CodeCat = categorie.CodeCat
JOIN habitant ON trophee.NumPreneur = habitant.NumHab;


SELECT "Nom des habitants qui habitent à Aquilona. (7 lignes)" AS "Question 8";
SELECT habitant.Nom, village.NomVillage FROM habitant
JOIN village ON habitant.NumVillage = village.NumVillage
WHERE village.NomVillage = "Aquilona";


SELECT "Nom des habitants ayant pris des trophées de catégorie Bouclier de Légat. (2 lignes)" AS "Question 9";

SELECT habitant.Nom FROM habitant
JOIN trophee ON habitant.NumHab = trophee.NumPreneur
JOIN categorie ON trophee.CodeCat = categorie.CodeCat
WHERE categorie.NomCateg = "Bouclier de Légat";


SELECT "Liste des potions (libellés) fabriquées par Panoramix : libellé, formule et constituant
principal. (3 lignes)" AS "Question 10";

SELECT potion.LibPotion, potion.Formule, potion.ConstituantPrincipal FROM potion
JOIN fabriquer ON potion.NumPotion = fabriquer.NumPotion
JOIN habitant ON fabriquer.NumHab = habitant.NumHab
WHERE habitant.Nom = "Panoramix";


SELECT "Liste des potions (libellés) absorbées par Homéopatix. (2 lignes)" AS "Question 11";

SELECT potion.LibPotion FROM potion
JOIN absorber ON potion.NumPotion = absorber.NumPotion
JOIN habitant ON absorber.NumHab = habitant.NumHab
WHERE habitant.Nom = "Homéopatix";

SELECT "Liste des habitants (noms) ayant absorbé une potion fabriquée par l'habitant numéro
3. (4 lignes)" AS "Question 12";

SELECT DISTINCT habitant.Nom FROM habitant
JOIN absorber ON habitant.NumHab = absorber.NumHab
JOIN fabriquer ON absorber.NumPotion = fabriquer.NumPotion
WHERE fabriquer.NumHab = 3;

SELECT "Liste des habitants (noms) ayant absorbé une potion fabriquée par Amnésix. (7 lignes)" 
AS "Question 13";

SELECT DISTINCT habitant.Nom FROM habitant
JOIN absorber ON habitant.NumHab = absorber.NumHab
JOIN fabriquer ON absorber.NumPotion = fabriquer.NumPotion
WHERE absorber.NumPotion IN
(SELECT fabriquer.NumPotion FROM fabriquer JOIN habitant ON fabriquer.NumHab = habitant.NumHab
WHERE habitant.Nom = "Amnésix");


SELECT "Nom des habitants dont la qualité n'est pas renseignée. (3 lignes)" AS "Question 14";

SELECT Nom FROM habitant
WHERE NumQualite is NULL;


SELECT "Nom des habitants ayant consommé la potion magique n°1 (c'est le libellé de la
potion) en février 52. (3 lignes)" AS "Question 15";

SELECT habitant.Nom FROM habitant
JOIN absorber ON habitant.NumHab = absorber.NumHab
WHERE absorber.NumPotion = 1 AND absorber.DateA BETWEEN "2052-02-01" AND "2052-02-29";


SELECT "Nom et âge des habitants par ordre alphabétique. (22 lignes)" AS "Question 16";

SELECT Nom, Age FROM habitant
ORDER BY Nom;

SELECT "Liste des resserres classées de la plus grande à la plus petite : nom de resserre et nom
du village. (3 lignes)" AS "Question 17";

SELECT resserre.NomResserre, resserre.Superficie, village.NomVillage FROM resserre
JOIN village on resserre.NumVillage = village.NumVillage
ORDER BY resserre.Superficie DESC;


SELECT "Nombre d'habitants du village numéro 5. (4)" AS "Question 18";

SELECT COUNT(NumHab) FROM habitant
WHERE NumVillage=5;


SELECT "Nombre de points gagnés par Goudurix. (5)" AS "Question 19";

SELECT SUM(categorie.NbPoints) from categorie
JOIN trophee on categorie.CodeCat = trophee.CodeCat
JOIN habitant ON trophee.NumPreneur = habitant.NumHab
WHERE habitant.Nom = "Goudurix";


SELECT "Date de première prise de trophée. (03/04/52)" AS "Question 20";

SELECT DatePrise FROM trophee
ORDER BY DatePrise
LIMIT 1;


SELECT "Nombre de louches de potion magique n°2 (c'est le libellé de la potion) absorbées. (19)"
AS "Question 21";
SELECT SUM(Quantite) FROM absorber
WHERE NumPotion = 2;


SELECT "Superficie la plus grande. (895)" AS "Question 22";
SELECT MAX(Superficie) FROM resserre;

SELECT "Nombre d'habitants par village (nom du village, nombre). (7 lignes)" AS "Question 23";

SELECT COUNT(habitant.NumHab) as "Nombre Habitant", village.NomVillage FROM habitant
JOIN village ON habitant.NumVillage = village.NumVillage
GROUP BY village.NomVillage;


SELECT "Nombre de trophées par habitant (6 lignes)" AS "Question 24";

SELECT COUNT(trophee.NumPreneur) AS "Nombre Trophee", habitant.Nom from trophee
JOIN habitant on trophee.NumPreneur = habitant.NumHab
GROUP BY habitant.Nom
ORDER BY COUNT(trophee.NumPreneur) DESC;

SELECT "Moyenne d'âge des habitants par province (nom de province, calcul). (3 lignes)" 
AS "Question 25";

SELECT AVG(habitant.Age) AS "Moyenne d'age", province.NomProvince from habitant
JOIN village ON habitant.NumVillage = village.NumVillage
JOIN province ON village.NumProvince = province.NumProvince
GROUP BY province.NomProvince;


SELECT "Nombre de potions différentes absorbées par chaque habitant (nom et nombre). (9
lignes)" AS "Question 26";

SELECT habitant.Nom, COUNT(DISTINCT absorber.NumPotion) AS "Nombre potions bu" from habitant
JOIN absorber ON habitant.NumHab = absorber.NumHab
GROUP BY habitant.Nom;


SELECT "Nom des habitants ayant bu plus de 2 louches de potion zen. (1 ligne)" AS "Question 27";

SELECT habitant.Nom, absorber.quantite from habitant
JOIN absorber ON habitant.NumHab = absorber.NumHab
JOIN potion on absorber.NumPotion = potion.NumPotion
WHERE absorber.quantite > 2 AND potion.LibPotion = "Potion Zen";


SELECT "Noms des villages dans lesquels on trouve une resserre (3 lignes)" AS "Question 28";

SELECT village.NomVillage, resserre.NomResserre FROM resserre
JOIN village on resserre.NumVillage = village.NumVillage;


SELECT "Nom du village contenant le plus grand nombre de huttes. (Gergovie)" AS "Question 29"; 

SELECT NomVillage, NbHuttes FROM village
ORDER BY NbHuttes DESC
LIMIT 1;

SELECT NomVillage, NbHuttes FROM village
WHERE NbHuttes = (SELECT MAX(NbHUttes) FROM village);


SELECT "Noms des habitants ayant pris plus de trophées qu'Obélix (3 lignes)" AS "Question 30";

SELECT COUNT(trophee.NumPreneur) AS winners, habitant.Nom from trophee
JOIN habitant on trophee.NumPreneur = habitant.NumHab
GROUP BY habitant.Nom
HAVING winners>
(SELECT COUNT(trophee.NumPreneur) FROM trophee
JOIN habitant ON trophee.NumPreneur = habitant.NumHab
WHERE habitant.Nom = "Obélix");





















